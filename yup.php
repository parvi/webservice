<?php
 header("Access-Control-Allow-Origin: *");


    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
   
require 'vendor/autoload.php';
require 'lib/mysql1.php';
 
$app = new \Slim\App;
 
$app->get('/sondages', 'get_sondages');
$app->get('/fiches', 'get_fiches');
 
$app->get('/questions/{id}', function($request, $response, $args) {
    get_questions($args['id']);
}); 
$app->get('/medias', 'get_medias');
$app->get('/pointventes', 'get_pointventes');


$app->run();
 
function get_sondages() {
    $db = connect_db();
    $sql = "SELECT * FROM sondages ORDER BY `sondage`";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    echo json_encode($data);
}

function get_fiches() {
    $db = connect_db();
    $sql = "SELECT * FROM fichevisites WHERE 1";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    echo json_encode($data);
}

function get_questions($idsondage) {
    $db = connect_db();
    $sql = "SELECT * FROM questionsondage WHERE  `idsondages` = '$idsondage' ORDER BY `dateAjout` DESC";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    echo json_encode($data);
}

function get_medias() {
    $db = connect_db();
    $sql = "SELECT * FROM mediatheques ORDER BY `media`";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    echo json_encode($data);
}

function get_pointventes() {
    $db = connect_db();
    $sql = "SELECT * FROM pointventes,utilisateurs_pointventes WHERE `pointventes`.`idpointVentes`=`utilisateurs_pointventes`.`idpointVentes` AND  idutilisateurs =".$_GET['idutilisateurs'];
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    echo json_encode($data);
}
 
?>