<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
   
require 'lib/mysql1.php';

$db = connect_db();
/*   $sql = "SELECT * FROM fichevisites WHERE 1";
    $exe = $db->query($sql);
    $data = $exe->fetch_all(MYSQLI_ASSOC);
    $db = null;
    print json_encode($data);
*/
  //  var_dump($data);

//$sql = "SELECT * FROM pointventes,utilisateurs WHERE idutilisateurs =".$_GET['idutilisateurs'];
$sql = "SELECT * FROM fichevisites,utilisateurs_pointventes,pointventes  WHERE `fichevisites`.`idUtilisateursPointVente`=`utilisateurs_pointventes`.`idUtilisateursPointVente` AND `utilisateurs_pointventes`.`idpointVentes`=`pointventes`.`idpointVentes` AND  idutilisateurs =".$_GET['idutilisateurs'] ;

$result = $db->query($sql);
$outp = "";
/*
    idficheVisites  idUtilisateursPointVente    photoPoint  brandingExterieur   brandingInterieur   grilleTarifVisible  formulaireClient    flyers  montantDeposit  niveauBatterieTPE   bracheSecteur   dispositionTPE  connaissanceCodeEmployeTPE  niveauFormation presenceConcurence  actionAMener    dateAjout   codePointVente  longitude   latitude    qrcode
    */
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"photoPoint":"'  . $rs["photoPoint"] . '",';
    $outp .= '"brandingExterieur":"'  . $rs["brandingExterieur"] . '",';
    $outp .= '"pointVente":'  .json_encode(utf8_decode($rs["pointVente"])) .',';
    $outp .= '"brandingInterieur":"'  . $rs["brandingInterieur"] . '",';
    $outp .= '"grilleTarifVisible":"'  . $rs["grilleTarifVisible"] . '",';
    $outp .= '"formulaireClient":"'  . $rs["formulaireClient"] . '",';
    $outp .= '"flyers":"'  . $rs["flyers"] . '",';
    $outp .= '"montantDeposit":"'  . $rs["montantDeposit"] . '",';
    $outp .= '"niveauBatterieTPE":"'  . $rs["niveauBatterieTPE"] . '",';
    $outp .= '"bracheSecteur":"'  . $rs["bracheSecteur"] . '",';
    $outp .= '"dispositionTPE":"'  . $rs["dispositionTPE"] . '",';
    $outp .= '"connaissanceCodeEmployeTPE":"'  . $rs["connaissanceCodeEmployeTPE"] . '",';
    $outp .= '"niveauFormation":"'  . $rs["niveauFormation"] . '",';
    $outp .= '"presenceConcurence":"'  . $rs["presenceConcurence"] . '",';
    $outp .= '"actionAMener":"'  . $rs["actionAMener"] . '",';
    $outp .= '"client":'  . json_encode(utf8_decode($rs["client"])) . ',';
    $outp .= '"dateAjout":"'  . $rs["dateAjout"] . '"}';
   // $outp .= '"qrcode":"'  . $rs["brandingExterieur"] . '",';

 //   $outp .= '"codePointVente":"'  . $rs["codePointVente"] . '"}';

}
$outp ='['.$outp.']';
$db->close();
//echo ($outp);

echo ($outp);




?>